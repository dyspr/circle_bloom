var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var steps = 32

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var j = 0; j < 6; j++) {
    push()
    translate(boardSize * 0.2 * sin(Math.PI * (j / 6) * 2), boardSize * 0.2 * cos(Math.PI * (j / 6) * 2))
    for (var i = 0; i < steps * abs(sin(frameCount * 0.05 + j * frameCount % 100 / steps + Math.random() * 0.5)); i++) {
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5)
      stroke(255 * abs(cos((i - 1) / steps * Math.PI + frameCount * 0.1 + j)), 0.75)
      strokeWeight(boardSize * 0.002)
      noFill()
      ellipse(0, 0, boardSize * (1 - i / steps) * 0.4)
      pop()
    }
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
